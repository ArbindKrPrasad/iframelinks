function newSite() {
    mid = "picasso";
    // let mid = document.getElementById("mid").value;
    let cid = document.getElementById("cid").value;
    let unifiedCid = cid + "_unifiedCode";
    let viewType = document.getElementById("viewType").value;
    let mandate = document.getElementById("mandate").value;
    let redirectionType = document.getElementById("redirectionType").value;
    let inputwidth = "900px";
    let inputheight = "700px";
    let getframe = document.getElementById("merchantViewID1");
    var now = Date.now() / 1000;
    let currentDate = new Date();
    let thirtyDayAhead = new Date(currentDate);
    thirtyDayAhead.setDate(thirtyDayAhead.getDate() + 30);
    let thirtyDayAheadEpoch = thirtyDayAhead.getTime() / 1000;

    console.log("Client ID: " + cid);
    console.log("Mandate: " + mandate);
    console.log("Redirection Type: " + redirectionType);

    if (viewType == "mobile") {
        inputwidth = "400px";
        inputheight = "800px";
        var bottom = document.getElementById("merchantViewID1");
        bottom.scrollIntoView();
        let mobileView = document.getElementById("merchantViewID1");
        mobileView.style.border = "5px solid #0b5ed7";
        mobileView.style.borderRadius = "10px";
        mobileView.style.marginTop = "10px";
    }
    if (viewType == "web") {
        let webView = document.getElementById("merchantViewID1");
        webView.style.border = "0px solid #0b5ed7";
    }
    getframe.style.width = inputwidth;
    getframe.style.height = inputheight;

    //API fetch starts here.
    var myHeaders = new Headers();
    myHeaders.append("version", "2021-06-01");
    myHeaders.append("Content-Type", "application/x-www-form-urlencoded");
    if (mid != "") {
        myHeaders.append("merchantid", mid);
    }
    myHeaders.append(
        "Authorization",
        "Basic OUU4QkUyMEU2NjM0OUJDQTQzMEM2RkFDMjcyQjM5"
    );

    let random = Math.floor(Math.random() * 1000000000);
    let ordId = "Test_" + random;
    var urlencoded = new URLSearchParams();
    urlencoded.append("order_id", ordId);
    urlencoded.append("amount", "1");
    urlencoded.append("currency", "INR");
    urlencoded.append("customer_id", "1234567899");
    urlencoded.append("customer_phone", "7017598190");
    urlencoded.append("udf3", "1234");
    urlencoded.append("metadata.remarks", "creating order");
    urlencoded.append("payment_page.client_id", cid);
    if (redirectionType == "iframe") {
        urlencoded.append("payment_page.sdk_payload", '{"integrationType":"iframe"}');
    } else {
        urlencoded.append("payment_page.sdk_payload", '{"integrationType":"redirection"}');
    }
    urlencoded.append("payment_page.environment", "sandbox");
    urlencoded.append("return_url", "https://www.icicilombard.com");
    urlencoded.append("options.create_mandate", mandate);
    urlencoded.append("mandate_max_amount", "100");
    urlencoded.append("mandate.end_date", thirtyDayAheadEpoch);
    urlencoded.append("mandate.frequency", "DAILY");
    urlencoded.append("mandate.start_date", now);
    if (mid == "picasso") {
        urlencoded.append("metadata.JUSPAY:gateway_reference_id", "atmandate");
    }


    var requestOptions = {
        method: "POST",
        headers: myHeaders,
        body: urlencoded,
        redirect: "follow",
    };

    fetch(
            "https://peaceful-headland-34518.herokuapp.com/https://sandbox.juspay.in/orders",
            requestOptions
        )
        .then((response) => response.json())
        .then((result) => {
            if (redirectionType.value == "redirect") {
                console.log("Link: " + result.payment_links.web);
                window.open(result.payment_links.web, "_blank");
            } else {
                console.log("Link: " + result.payment_links.web);
                document.getElementById("merchantViewID1").src =
                    result.payment_links.web;
            }
        });

    console.log("Client ID: " + cid);
    console.log("Mandate: " + mandate);
    console.log("Redirection Type: " + redirectionType);

}